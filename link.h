﻿#pragma once
#include <string>

using namespace std;

class Link
{
public:
	virtual void print() = 0;
	virtual string getLinkPath() = 0;
	virtual void setLinkPath(string fp) = 0;
	virtual string getLinkName() = 0;
	virtual void setLinkName(string fn) = 0;
};
//�UKASZ PACHOLEC
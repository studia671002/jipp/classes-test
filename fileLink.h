﻿#pragma once
#include "link.h"
#include <string>

using namespace std;

class FileLink :public Link
{
	string fLinkPath;
	string fLinkName;

public:
	FileLink();
	FileLink(string fl);

	string getDestinationExtenstion();
	string getDestinationFileName();

	virtual void print();
	virtual string getLinkPath();
	virtual void setLinkPath(string fp);
	virtual string getLinkName();
	virtual void setLinkName(string fn);
};
//�UKASZ PACHOLEC
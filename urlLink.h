﻿#pragma once
#include "link.h"
#include <string>

using namespace std;

class UrlLink :public Link
{
	string siteUrl;
	string urlName;
public:
	UrlLink();
	UrlLink(string url);

	bool isHttps();

	virtual void print();
	virtual string getLinkPath();
	virtual void setLinkPath(string url);
	virtual string getLinkName();
	virtual void setLinkName(string name);
};
//�UKASZ PACHOLEC
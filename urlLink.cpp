﻿#include "urlLink.h"
#include <iostream>

using namespace std;

UrlLink::UrlLink() : siteUrl(""), urlName("basic")
{
}

UrlLink::UrlLink(string url) : urlName("basic")
{
	//TU POWINNO BY� ZABEZPIECZENIE PRZED PODANIEM B��DNEJ �CIE�KI
	this->siteUrl = url;
}

bool UrlLink::isHttps()
{
	string https = "https";
	for (int i = 0; i <= 4; i++) {
		if (siteUrl[i] != https[i])
			return false;
	}
	return true;
}

void UrlLink::print()
{
	cout << "Url path: " << urlName << " -> (" << siteUrl << ")" << endl;
}

string UrlLink::getLinkPath()
{
	return siteUrl;
}

void UrlLink::setLinkPath(string url)
{
	//TU POWINNO BY� ZABEZPIECZENIE PRZED PODANIEM B��DNEJ �CIE�KI
	this->siteUrl = url;
}

string UrlLink::getLinkName()
{
	return urlName;
}

void UrlLink::setLinkName(string name)
{
	this->urlName = name;
}
//�UKASZ PACHOLEC
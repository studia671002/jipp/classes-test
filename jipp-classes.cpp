﻿#include <iostream>
#include "link.h"
#include "fileLink.h"
#include "urlLink.h"

using namespace std;

bool compareLink(Link& left, Link& right) {
    left.print();
    right.print();
    return left.getLinkName() == right.getLinkName();
}

int main()
{
    FileLink ll("/test/lipa/");

    ll.print();

    ll.setLinkName("testing");
    ll.print();

    cout << endl;

    cout << "linkPath: " << ll.getLinkPath() << endl;
    cout << "extension: " << ll.getDestinationExtenstion() << endl;
    cout << "Name: " << ll.getDestinationFileName() << endl;

    cout << endl;
    cout << endl;

    ll.setLinkPath("/very/good/path/with/end.jpeg.zip");

    cout << "linkPath: " << ll.getLinkPath() << endl;
    cout << "extension: " << ll.getDestinationExtenstion() << endl;
    cout << "Name: " << ll.getDestinationFileName() << endl;

    cout << endl;

    UrlLink url("http://testowanie/tego/czegos");
    url.print();

    url.setLinkName("test");
    url.print();

    cout << endl;

    cout << "Is https? : " << url.isHttps() << endl;

    url.setLinkPath("https://testowanie/tego/czegos");

    cout << "Is https? : " << url.isHttps() << endl;
    cout << "Are links name equal? : " << compareLink(ll, url) << endl;

    cout << endl;


    ll.setLinkName("lukasz");
    url.setLinkName("lukasz");

    cout << "Are links name equal? : " << compareLink(url, ll) << endl;


    cout << endl;
    url.setLinkName("testujesSobie");

    cout << "Are links name equal? : " << compareLink(url, ll) << endl;
    return 0;
}
//�UKASZ PACHOLEC
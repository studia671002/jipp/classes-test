﻿#include "fileLink.h"
#include <iostream>

using namespace std;

FileLink::FileLink() : fLinkPath(""), fLinkName("basic")
{
}

FileLink::FileLink(string fl) : fLinkName("basic")
{
	//TU POWINNO BY� ZABEZPIECZENIE PRZED PODANIEM B��DNEJ �CIE�KI
	this->fLinkPath = fl;
}

string FileLink::getDestinationExtenstion()
{
	int dotIndex = -1;
	for (int i = fLinkPath.length() - 1; i >= 0; i--) {
		if (fLinkPath[i] == '.') {
			dotIndex = i;
			break;
		}
	}

	if (dotIndex == -1)
		return "";
	string extension;
	for (int i = dotIndex; i <= fLinkPath.length() - 1; i++) {
		if (fLinkPath[i] == '/') {
			break;
		}
		extension += fLinkPath[i];
	}

	return extension;
}

string FileLink::getDestinationFileName()
{
	int slashIndex = -1;
	for (int i = fLinkPath.length() - 1; i >= 0; i--) {
		if (fLinkPath[i] == '/') {
			slashIndex = i;
			break;
		}
	}

	string name;
	for (int i = slashIndex + 1; i <= fLinkPath.length() - 1; i++) {
		if (fLinkPath[i] == '.') {
			break;
		}
		name += fLinkPath[i];
	}

	return name;
}

void FileLink::print()
{
	cout << "File link path: " << fLinkName << " -> (" << fLinkPath << ")" << endl;
}

string FileLink::getLinkPath()
{
	return fLinkPath;
}

void FileLink::setLinkPath(string fp)
{
	//TU POWINNO BY� ZABEZPIECZENIE PRZED PODANIEM B��DNEJ �CIE�KI
	this->fLinkPath = fp;
}

string FileLink::getLinkName()
{
	return fLinkName;
}

void FileLink::setLinkName(string fn)
{
	this->fLinkName = fn;
}
//�UKASZ PACHOLEC
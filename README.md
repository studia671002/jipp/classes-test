Poniższe zadanie zrealizuj w Visual Studio z wykorzystaniem jego systemu zarządzania projektem (nie używamy tutaj cmake).

Po kolokwium należy odesłać spakowany cały folder projektu, instrukjca znajduje się na delcie.

Zadania przesłane po czasie, które nie zdążą się zmieścić w rezerwie czasowej na opóźnienie nie będą sprawdzane, tak samo, jak zadania, które się nie kompilują.

 

Stwórz czysto wirtualną klasę Link, która zawiera publiczne metody print, getLinkPath, setLinkPath, getLinkName oraz setLinkName.
Stwórz klasę FileLink bazującą na Link. Będzie ona służyła do przechowywania ścieżki do pliku oraz posiadać funkcjonalności:
Konstruktor bez parametryczny - pusta ścieżka.
Konstruktor parametryczny, podawana jest ścieżka do pliku.
Metode getDestinationExtension, zwracająca rozszerzenie pliku (znaki po ostatniej kropce w nazwie, jeżeli jej nie ma, to pusty tekst).
Metode getDestinationFileName, zwraca nazwę pliku bez rozszerzenia (patrz wyżej) oraz bez ścieżki do pliku.
Metoda print wypisuje na ekran File link path: <nazwa linku> ->(<ścieżka do pliku>).
Stwórz klasę UrlLink bazującą na Link. Będzie ona służyła do przechowywania adresu strony internetowej oraz posiadać funkcjonalności:
Konstruktor bez parametryczny - pusty adres.
Konstruktor parametryczny, podawany jest adres do strony.
Metoda print wypisuje na ekran Url path: <nazwa linku> ->(<adres strony>).
Metoda isHttps zwraca wartość bool w zależności od tego, czy link zaczyna się od https.
Stwórz funkcję compareLink, która może przyjmować dowolnie FileLink oraz UrlLink, jako pierwszy oraz drugi parametr (w pierwszym parametrze może zostać przekazane FilePath lub DirPath i w drugim tak samo). Funkcja na początku będzie używać metody print z obydwóch przekazanych parametrów, a następnie będzie zwracać wartość logiczną w zależności, czy obydwa linki mają taką samą nazwę.
W ostatniej linii każdego pliku wpisz swoje imię i nazwisko.
Zadanie powinno posiadać poprawny podział na pliki nagłówkowe i źródłowe.
Można tworzyć dowolne dodatkowe metody w klasach.
 

Na potrzeby powyższego zadania ustalamy:

Ścieżki są w systemie unixowym, czyli separację pomiędzy poszczególnymi folderami wykonujemy znakiem /.
Nazwa pliku nie może zawierać znaku /.
Przechowujemy tylko ścieżki absolutne (względem podstawy systemu plików, a nie względem aktualnego folderu).
Program powinien byc zabezpieczony przed przekazaniem błędnej ścieżki. W miejscu, w ktorym napisałbyś takie zabezpieczenie stwórz komentarz mówiący, że tutaj ono powinno być.
Przykładowy adres strony http://pk.edu.pl
Dodatkowe informacje:

Wykrycie ewidentnego plagiatu lub wspomaganie się innymi gotowymi rozwiązaniami powyższego zadania będzie równoznaczne z oceną 2.0
Podczas kolokwium nie można korzystać z dodatkowych materiałów. Można korzystać jedynie z informacji, które można znaleźć w Visual Studio i poniżej.
 